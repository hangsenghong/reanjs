var app = new Vue({
    el:'#app',
    data: {
        name:'hello senghong',
        age:'23',
        website:'https://www.youtube.com',
        url:'<a href="https://www.youtube.com">visit youtube</a>',
        seen:true,
        message:'hello I am Messager !',
        product:'panda black',
        image:'./assets/image/th.webp'
    },
    methods: {
        greeting: function(time){
            return time+' '+this.name;
        },
        reverMessage:function(){
            this.message=this.message.split('').reverse().join('')
        }
    }

})
// app.seen=false;
// var for list.hmtl
var list = new Vue({
    el:'#app-list',
    data:{
        todos:[
            {text:'learn javascript '},
            {text: 'Learn Vue'},
            {text: 'Biuld somthing awesome'}
        ]
    }
})
// Vue component
Vue.component('plan',{
    template:'#plan-template'
})