// components
Vue.component('task', {
    template: '<li><slot></slot></li>'
});

Vue.component('task-list', {
    template: `
    <div>
            
            <task v-for="task in tasks">{{task.task}}</task>
            
            </div>`,


    data() {
        return {
            tasks: [
                { task: 'go to the store ', complete: true },
                { task: 'go to the home ', complete: true },
                { task: 'go to the pagoda ', complete: true },
                { task: 'go to the school ', complete: true },
                { task: 'go to the market ', complete: false },
            ]
        }

    }
});
new Vue({
    el: '#comp'
})

Vue.component('message', {
    props: ['title', 'body'],
    data() {
        return {
            isVisible: true,
        };
    },

    template: `
    
    <article class="message" v-show="isVisible">
            <div class="message-header">
              {{title}}
              <button class="" @click="isVisible = false">x</button>
            </div>
            <div class="message-body">
            {{body}}
            </div>
          </article>
          `,
    methods: {
        hideModel() {
            $('.message').hide;
        }
    }
});

new Vue({
    el: '#comp1',
});
// =========Props=======//
var pp = new Vue({
    el: '#pp',
    data: {
        
    }
})
